package com.example.gapsi.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.gapsi.R;
import com.example.gapsi.model.Record;
import com.example.gapsi.network.UserClient;
import com.example.gapsi.network.UserInterface;
import com.example.gapsi.view.adapters.ProductoAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    UserInterface userInterface;
    UserClient userClient;
    private EditText etSearch;
    private ImageButton ibSearch;
    private List<Record> products = new ArrayList<>();

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retrofitInit();
        initComponents();

    }
    private void initComponents(){
        etSearch = findViewById(R.id.etSearch);
        ibSearch = findViewById(R.id.ibSearch);
        recyclerView = findViewById(R.id.rvProducts);
        ibSearch.setOnClickListener(this);



    }

    private void retrofitInit() {
        userClient = UserClient.getInstance();
        userInterface = userClient.getAuthApiService();
    }

    @Override
    public void onClick(View v) {
        int id  = v.getId();
        switch (id){
            case R.id.ibSearch:
                fetchData();
                break;
        }
    }

    private void fetchData(){
        String toSearch = etSearch.getText().toString();
        int page = 1;
        int items = 10;

        Call<com.example.gapsi.model.Response> call = userInterface.getData(toSearch, page, items);
        call.enqueue(new Callback<com.example.gapsi.model.Response>() {
            @Override
            public void onResponse(Call<com.example.gapsi.model.Response> call, Response<com.example.gapsi.model.Response> response) {
                if (response.isSuccessful()){
                    response.body();
                   products =  response.body().getPlpResults().getRecords();
                    recyclerView.setHasFixedSize(true);

                    // use a linear layout manager
                    layoutManager = new LinearLayoutManager(MainActivity.this);
                    recyclerView.setLayoutManager(layoutManager);

                    // specify an adapter (see also next example)
                    mAdapter = new ProductoAdapter(products, MainActivity.this);
                    recyclerView.setAdapter(mAdapter);
                }else{
                    response.errorBody();
                }
            }

            @Override
            public void onFailure(Call<com.example.gapsi.model.Response> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    private void fillList(List<Record> products){
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new ProductoAdapter(products, this);
        recyclerView.setAdapter(mAdapter);
    }
}
