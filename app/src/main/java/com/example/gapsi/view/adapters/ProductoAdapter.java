package com.example.gapsi.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gapsi.R;
import com.example.gapsi.model.Record;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductoAdapter extends RecyclerView.Adapter<ProductoAdapter.ViewHolder> {

    private List<Record> mData;
    private LayoutInflater mInflater;
    private Context context;


    // data is passed into the constructor
    public ProductoAdapter(List<Record> data, Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_product, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Record product = mData.get(position);
        holder.tvProductName.setText(product.getProductDisplayName());
        holder.tvProductPrice.setText(product.getListPrice().toString());
        holder.tvProductLocation.setText(product.getProductType());
        Picasso.with(holder.ivProducImage.getContext()).load(product.getLgImage()).into(holder.ivProducImage);

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvProductName, tvProductLocation, tvProductPrice;
        ImageView ivProducImage;

        ViewHolder(View itemView) {
            super(itemView);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvProductLocation = itemView.findViewById(R.id.tvProductLocation);
            tvProductPrice = itemView.findViewById(R.id.tvProductPrice);
            ivProducImage = itemView.findViewById(R.id.ivProductImage);
            // itemView.setOnClickListener(this);
        }

    }

}