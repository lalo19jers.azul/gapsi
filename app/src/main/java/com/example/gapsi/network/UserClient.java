package com.example.gapsi.network;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserClient {
    private static final String BASE_URL = "https://shoppapp.liverpool.com.mx/appclienteservices/services/v3/";
    private static Retrofit retrofit;
    private UserInterface userInterface;
    private static UserClient instance = null;

    public UserClient() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                .readTimeout(3, TimeUnit.MINUTES)
                .writeTimeout(3, TimeUnit.MINUTES)
                .connectTimeout(3, TimeUnit.MINUTES);
       // okHttpClientBuilder.addInterceptor(new AuthInterceptor());
        OkHttpClient client = okHttpClientBuilder.build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        userInterface = retrofit.create(UserInterface.class);
    }

    /**
     * Singleton Method
     * @return
     */
    public static UserClient getInstance() {
        if (instance == null) {
            instance = new UserClient();
        }

        return instance;
    }

    /**
     *
     */
    public UserInterface getAuthApiService() {
        return userInterface;
    }


}
