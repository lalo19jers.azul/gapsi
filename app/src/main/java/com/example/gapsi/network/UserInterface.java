package com.example.gapsi.network;



import com.example.gapsi.model.Response;

import org.json.JSONObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface UserInterface {

    /*
   Get request to fetch city weather.Takes in two parameter-city name and API key.
   */
    @GET("plp?force-plp=true")
   // Call<JSONObject> getData(@Path("search-string") String object, @Path("page-number") int page, @Path("number-of-items-per-page") int items);
    // Call<JSONObject> getData(@QueryMap(encoded=true) Map<String, String> options);
     Call<Response> getData(@Query("search-string") String object, @Query("page-number") int page, @Query("number-of-items-per-page") int items);
}
